import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import React from "react";
import Col from "react-bootstrap/Col";
import styled from "styled-components";
import Button from "react-bootstrap/Button";

const projects = [
    {
        name: "Devblog",
        description: "This site. It includes a Flask backend using Postgres + Sqlalchemy " +
            "and a React frontend. The full source code is available on Gitlab if you need " +
            "examples of working code or are just curious.",
        link: "https://gitlab.com/devblog",
        demo: "/editordemo",
    },
    {
        name: "Rye",
        description: "A fully configurable Python test/task runner, without coding. Leverages" +
            " pyproject.toml and allows you to specify tasks and environments for those tasks. " +
            "The tool handles ensuring that virtualenvs are properly configured and tasks are " +
            "run in parallel. Compare to Tox.",
        link: "https://rye.readthedocs.io",
    },
    {
        name: "Settingscascade",
        description: "Parse config files with all the power of CSS. Lets you layer config from " +
            "multiple locations, using inheritance and composition to simplify the process for " +
            "your users to configure your application, while keeping the code in the app itself " +
            "much more simple.",
        link: "https://settingscascade.readthedocs.io",
    },
    {
        name: "Contractual",
        description: "Pact compatible native Python contract testing library (DEVELOPMENT). The " +
            "goal here is to build a library that can test Python objects natively (instead of " +
            "requiring the external http server)- while still creating and verifying contract files " +
            "for downstream systems. Can even be useful within a project to verify complicated mocks." +
            "This is under heavy development.",
        link: "https://gitlab.com/pjbecotte/contractual",
    },
    {
        name: "Digitalocean Volume Plugin",
        description: "Docker volume plugin for digital ocean block volumes",
        link: "https://gitlab.com/pjbecotte/digitalocean_volume_plugin",
    }
];

const AboutContainer = styled(Container)`
    background-color: ${(props) => props.theme.primaryColor};
    opacity: ${(props) => props.theme.menuOpacity};
    color: ${(props) => props.theme.textColor};
    padding: 10px;
    margin-top: 25px;
    border-radius: 15px;
`;

const StyleHeader = styled(Row)`
  margin-top: 25px;
`;

const ProjectDiv = styled(Row)`
  margin: 10px;
  background-color: #5a6268;
  border-radius: 15px;
  padding: 25px 10px;
`;

export default () => {
    return (
        <AboutContainer>
            <StyleHeader>
                <Col sm={{offset: 2, span: 8}}>
                    <h1>Hi, I'm Paul Becotte!</h1>
                </Col>
            </StyleHeader>
            <Row>
                <Col sm={{offset: 2, span: 8}}>
                    <p>
                        I am an obsessive technologist who lives to build distributed systems
                        that are fault tolerant, can scale horizontally, and provide easy
                        operations and development. I prefer to build something and try it out
                        versus imagining I can perfect it in a vacuum. I believe that
                        investments in work processes have an immediate positive return on
                        investment. I am at my best when solving new problems that require
                        integrating a bunch of tools that were never meant to work that way.
                        My career so far has included creating DevOps at a company with no
                        DevOps, going from developer to CTO at an ad-tech startup, and
                        working on automation and tooling at a several financial companies. I
                        haven’t yet met the problem that didn’t interest me.
                    </p>
                </Col>
            </Row>
            <Row>
                <Col sm={{offset: 2, span: 8}}>
                    <Button variant="secondary" block href={`${process.env.PUBLIC_URL}/PaulBecotte.pdf`}>
                        Resume
                    </Button>
                </Col>
            </Row>
            <hr/>
            <StyleHeader>
                <Col sm={{offset: 4, span: 8}}>
                    <h2>Projects</h2>
                </Col>
            </StyleHeader>
            {projects.map((project) => {
                return (
                    <ProjectDiv>
                        <Col sm={{offset: 1, span: 4}}>
                            <Row className="justify-content-center">
                                <h4>{project.name}</h4>
                            </Row>
                            <Row>
                                <Button variant="primary" block target="_blank" href={project.link}>Link</Button>
                            </Row>
                            {project.demo ? (
                                <Row>
                                    <Button variant="primary" block href={project.demo}>Demo</Button>
                                </Row>
                            ) : ""}
                        </Col>
                        <Col sm={7}>
                            <p>{project.description}</p>
                        </Col>
                    </ProjectDiv>
                );
            })}

        </AboutContainer>
    )
}