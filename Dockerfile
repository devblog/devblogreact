# Stage 0, "build-stage", based on Node.js, to build and compile the frontend
FROM node:14 as node
WORKDIR /app
COPY package.json yarn.lock /app/
RUN yarn install
COPY ./ /app/
RUN npm run build
# Stage 1, based on Nginx, to have only the compiled app, ready for production with Nginx
FROM nginx:1.15
COPY --from=node /app/build/ /usr/share/nginx/html
COPY  conf/nginx.conf /etc/nginx/conf.d/default.conf
